import os
import numpy as np
import astropy.io.fits as pyfits
from utils import *
import copy

class xtegen_mch(object):
    """
    Object for generating XTE lighcurves in multiple channel ranges.
    For calculation of cross-correlation, etc.

    """
    
    def __init__(
        self, 
        info,
        fft_modes,
        bin_size,
        interval,
        channels=[(0,255)],
        expr="ELV > 5.0 and OFFSET < 0.1 and NUM_PCU_ON > 0",
        dtcor=True,
        verbosity = 0,
    ):
        self.__dict__ = copy.copy(info)
        self.fft_modes = fft_modes
        self.expr = expr
        self.dtcor = dtcor
        self.verbosity = verbosity
        self.interval = interval
        self.channels = channels
        if self.expr.strip() =="":
            self.expr = "True"
            
        self.bin_size = bin_size
        self.initialize()
        #if self.dtcor:
        self.init_dt()
        #self.total_time = self.ftime[-1] - self.ftime[0]

        
    def __repr__(self):
        return f'XTE observation {self.id}'

    # Python 3 compatibility
    def __next__(self):
        
        tarr, arr = self.get_next_interval()
        if not self.skip:
            tdt = self.get_next_dt()
        else:
            tdt = self.time
        self.time = np.max([tarr,tdt])

        return arr
    
    def initialize(self):
        """ XTE data stream generator. """
        
        maxres = self.tres[self.fft_modes[0]]
        for m in self.fft_modes:
            maxres = max(maxres,self.tres[m])
            
        if maxres > self.bin_size:
            self.bin_size = maxres
            if self.verbosity > 0:

                print("WARNING: Time resolution of the data modes %f sec. is lower than requested"%maxres)
                print("Setting time bin size to %f"%maxres)
            
        self.array_size = int(self.interval/self.bin_size)
        self.stopped = False
        self.filtgti = makegti(self.filter,self.expr)
        self.time = self.filtgti[0][0]
        self.nphotons = 0

        try:

            gtif = pyfits.open(self.gti)
            self.have_gti = True
            self.gtistart = gtif[1].data.field('START')
            self.gtistop = gtif[1].data.field('STOP')

        except:
  
            self.have_gti = False

        self.fft_file_number = np.zeros(len(self.fft_modes),dtype=int)
        self.files = []
        self.fnames = []

        for m in self.fft_modes:
            if self.verbosity>0: 
                print("Opening file:",os.path.basename(self.pcadata[m][0])," mode", m)
            self.files.append(pyfits.open(self.pcadata[m][0],memmap=True))
            self.fnames.append(self.pcadata[m][0])

        self.gti_ind = int(0)
        self.dataind = np.zeros(len(self.files),dtype=int)

#    print tstart,tstop
    
        self.tstart = []
        self.tstop = []
        self.sci_mode = []
        self.numchan = []
        self.chns = []

        # filtering data channels here:
        for f in self.files:
            
            datamode = f[1].header['DATAMODE']
            self.tstart.append(f[1].data.field(0)[0])
            self.tstop.append(f[1].data.field(0)[-1])
            self.sci_mode.append(f[1].header['EXTNAME'])
            numch = self.nchan[datamode]
            self.numchan.append(numch)
            
            mode_chans = []
            for channel_range in self.channels:

                chans_in_range = []
                for ich in range(numch):
                    ch1 = self.pcachan_low[datamode][ich]
                    ch2 = self.pcachan_high[datamode][ich]

                    if ch1>=channel_range[0] and ch2<=channel_range[1]: 
                
                        chans_in_range.append(ich)
                if chans_in_range == []:
                    mode_chans.append((-1,-1))            
                else:
                    mode_chans.append((min(chans_in_range),max(chans_in_range)))
                    
            self.chns.append(mode_chans)
            
        #print(self.chns)
        #print(self.channels)
        self.skip = int(0)
        self.skip_message = ''

        if not os.path.exists(self.filter):
            
            print(f"Filter file {self.filter} is not found!")
            print("No deadtime correction or good time intervals filtering will be done.")
            self.dtcor = False
      

    def get_next_interval(self):
        """ XTE data stream generator. """
        
        if self.stopped:
            raise StopIteration()
            
        gti = self.have_gti
        gtistart = self.gtistart
        gtistop = self.gtistop
            
        stopfft = False
        
        chans = self.channels
        filtgti = self.filtgti
        fft_int = self.interval
        time = copy.copy(self.time)
        fft_arr_size = self.array_size

        self.skip = int(0)
        self.skip_message = ''

        if time+fft_int > filtgti[1][self.gti_ind]:
            self.gti_ind += 1
            if self.gti_ind >= len(filtgti[1]):
                stopfft = True
            else:
                time = filtgti[0][self.gti_ind]
            self.skip = 4
            self.skip_message = "GTI changeover"

        #Making sure all datamodes are on during the interval

        for i in range(len(self.files)):
        
            if ( time <= self.tstart[i] ) or (self.time+fft_int >= self.tstop[i]+self.files[i][1].header['TIMEDEL']) :
                if self.verbosity>1: 
                    print("Mode",self.fft_modes[i],",file",os.path.basename(self.fnames[i])," does not cover interval.")
                self.skip = 1
                self.skip_message = "Data not available."

        ngamma = 0
        fft_arr = np.zeros((len(self.channels),self.array_size),dtype=float)
        #print(fft_arr.shape)

        # GTI block, checking if the data are entirely within  GTIs

        if gti:
            ingti = int(0)
            for j in range(len(self.gtistart)):
                if time >= self.gtistart[j] and time+fft_int <= self.gtistop[j]:
                    ingti += 1
            if ingti == 0:
                self.skip = 3
                self.skip_message = "Rejected due to GTIs."
                
        else:
            ingti = 1
        
    
        if not self.skip:

            for i in range(len(self.files)):
            
                times = self.files[i][1].data.field(0)
                ch_ranges = self.chns[i]
                #print(ch_ranges)

                while times[self.dataind[i]] < self.time: self.dataind[i] += 1
                while times[self.dataind[i]] > self.time: self.dataind[i] -= 1

                tsize = len(times)
            
                # Accumulating data Science Array formats (Binned and Single Bit)

                if self.sci_mode[i] == 'XTE_SA':
                    events = self.files[i][1].data.field(1)
                    ngr = int(self.files[i][1].header['TIMEDEL']/self.files[i][1].header['1CDLT2'])
                    b_ind = int(0)
                    st = max(1,int(self.bin_size/self.files[i][1].header['1CDLT2']))
                    
                    for irange in range(len(ch_ranges)):
                        ch_range = ch_ranges[irange]
                        #print(ch_range)
                        barr = np.zeros(ngr,dtype=float)

                        if self.numchan[i]>1 and ch_range != (-1,-1):
                            barr = np.sum(events[self.dataind[i]][ch_range[0]:ch_range[1]+1,:],axis=0)
                        elif ch_range != (-1,-1):
                            barr = events[self.dataind[i]]
                        for k in range(fft_arr_size):
                            fft_arr[irange,k] += np.sum(barr[b_ind:b_ind+st])
                            b_ind += st
                            if b_ind >= ngr:
                                b_ind = 0
                                self.dataind[i] += 1
                                if self.dataind[i] == len(events):
                                    print(f"Warning: dataind overflow for  mode {self.fft_modes[i]}.")
                                    print(f"dataind {self.dataind[i]}, len(events) = {len(events)}.")
                                    break
                                    
                                barr = np.zeros(ngr,dtype=float)
                                if self.numchan[i]>1 and ch_range != (-1,-1):
                                    barr = sum(events[self.dataind[i]][ch_range[0]:ch_range[1]+1,:],axis=0)
                                elif ch_range != (-1,-1):
                                    barr = events[self.dataind[i]]

                    # Accumulating data in Science Event format (Event)

                if self.sci_mode[i] == 'XTE_SE':
                    events = self.files[i][1].data.field('PHA')
                    for k in range(fft_arr_size):
                        t1 = self.time + float(k+1)*self.bin_size
                        while times[self.dataind[i]] < t1:
                            for irange in range(len(ch_ranges)):
                                
                                ch_range = ch_ranges[irange]
                                if events[self.dataind[i]] >= ch_range[0] and events[self.dataind[i]] <= ch_range[1]: fft_arr[irange,k] += 1
                            
                            self.dataind[i] += 1
                
        ngamma = np.sum(fft_arr)
        self.nphotons += ngamma

                
        time += fft_int
        #print('fft',time)
        for i in range(len(self.files)):
        
            if time >= self.tstop[i]:

                if self.verbosity > 1: 
                    print("Closing file:",os.path.basename(self.fnames[i]),". Mode:", self.fft_modes[i],".")
                self.files[i].close()
                modefiles = self.pcadata[self.fft_modes[i]]
                ind = modefiles.index(self.fnames[i])
#                print ind,len(modefiles)
                if ind == len(modefiles) - 1:
                    if self.verbosity > 1: 
                        print("Mode",fft_modes[i]," exhausted. Stoping!")
                    stopfft = 1
                else:
                    self.fnames[i] = modefiles[ind+1]
 
                    if self.verbosity > 1: 
                        print("Opening file:",os.path.basename(self.fnames[i]),". Mode:", self.fft_modes[i],".")
                    self.files[i] = pyfits.open(self.fnames[i],memmap=True)
                    self.tstart[i] = self.files[i][1].data.field(0)[0]
                    self.tstop[i] = self.files[i][1].data.field(0)[-1]
                    self.dataind[i] = 0
                    time = max(max(self.tstart),time)
                    #self.dataind = np.zeros(len(self.files),dtype=int)
                

        if stopfft:
            self.stopped = True
            
        return time, fft_arr

    def init_dt(self):
        
        self.filt = pyfits.open(self.filter)
        self.num_pcu_on = self.filt[1].data.field('NUM_PCU_ON')
        
        self.ftime = self.filt[1].data.field('Time')
        self.pcu2hk = pyfits.open(self.fh5c[0])
        self.pcu2hk_ind = int(0)
        self.dsvle = self.pcu2hk[1].data.field('dsvle')
        self.std1f = pyfits.open(self.pcadata['Standard1'][0])
        self.std1_file = int(0)
        self.std1t = self.std1f[1].data.field('Time')
        self.std1f_len = len(self.std1t)
        self.xepcu0 = self.std1f[1].data.field('XeCntPcu0')
        self.xepcu1 = self.std1f[1].data.field('XeCntPcu1')
        self.xepcu2 = self.std1f[1].data.field('XeCntPcu2')
        self.xepcu3 = self.std1f[1].data.field('XeCntPcu3')
        self.xepcu4 = self.std1f[1].data.field('XeCntPcu4')
        self.vpcnt = self.std1f[1].data.field('VpCnt')
        self.remcnt = self.std1f[1].data.field('RemainingCnt')
        self.vlecnt = self.std1f[1].data.field('VLECnt')
        self.std1_arr_ind = int(0)
        self.filt_ind = int(0)
        self.std1f_ind = int(0)
        self.dsvle_value = 0.0
        self.rxenon = 0.0
        self.rvle = 0.0
        self.pcuon = 0.0
        self.npcuon = int(0)
        self.td = 1.0e-5
        self.n_dsvle = int(0)
        self.cor_time = 0.0
        self.time = max(self.time,self.std1t[self.std1f_ind])

    def get_next_dt(self):
        
        """ Update deadtime parameters """

        fft_int = self.interval
        t = max(self.time,self.std1t[self.std1f_ind])

        
        if (t + fft_int >= self.std1t[-1]+128.0):
            self.skip = 2
            self.skip_message = "Std 1 data is not available"

        while self.std1t[self.std1f_ind]+0.125*self.std1_arr_ind < t - 0.125 and self.skip == 0:
            self.std1_arr_ind += 1
            if self.std1_arr_ind == 1023:
                self.std1f_ind += 1
                self.std1_arr_ind = 0

#   settting HK pointer
    
        while self.pcu2hk[1].data.field('Time')[self.pcu2hk_ind] < t-8.0: 
            self.pcu2hk_ind += 1

# Setting time pointer to filter file	
        while self.ftime[self.filt_ind]+0.125*self.std1_arr_ind < t: 
            self.filt_ind += 1
        
        
        #print(self.filt_ind,self.std1t[self.std1f_ind]+0.125*self.std1_arr_ind,t+fft_int)
        while self.std1t[self.std1f_ind]+0.125*self.std1_arr_ind < t+fft_int:
                

            while self.pcu2hk[1].data.field('Time')[self.pcu2hk_ind] < self.std1t[self.std1f_ind] + 0.125*self.std1_arr_ind - self.pcu2hk[1].header['TIMEDEL']:
                self.dsvle_value += self.dsvle[self.pcu2hk_ind]
                self.pcu2hk_ind += 1
                self.n_dsvle += 1

            while self.ftime[self.filt_ind] < self.std1t[self.std1f_ind]+0.125*self.std1_arr_ind - self.filt[0].header['TIMEDEL']:
                        #print(self.filt_ind,self.std1f_ind,self.std1_arr_ind,t)
                        self.filt_ind += 1
                        self.pcuon += self.num_pcu_on[self.filt_ind]
                        self.npcuon += 1
                        
#                	print pcuon, npcuon

            rtmp = 0.0
            if self.num_pcu_on[self.filt_ind] > 0:
                rtmp += self.xepcu0[self.std1f_ind][self.std1_arr_ind]
                rtmp += self.xepcu1[self.std1f_ind][self.std1_arr_ind]
                rtmp += self.xepcu2[self.std1f_ind][self.std1_arr_ind]
                rtmp += self.xepcu3[self.std1f_ind][self.std1_arr_ind]
                rtmp += self.xepcu4[self.std1f_ind][self.std1_arr_ind]
                rtmp += self.vpcnt[self.std1f_ind][self.std1_arr_ind]
                rtmp += self.remcnt[self.std1f_ind][self.std1_arr_ind]
                self.rxenon += rtmp
                self.rvle += self.vlecnt[self.std1f_ind][self.std1_arr_ind]

            self.std1_arr_ind += 1
            self.cor_time += 0.125
            if self.std1_arr_ind == 1023:
#             	print std1f_ind,pcu2hk[1].data.field('Time')[pcu2hk_ind],pcu2hk_ind,n_dsvle
                self.std1f_ind += 1
                self.std1_arr_ind = 0

        t += fft_int
        
        if (t + fft_int >= self.std1t[-1]) & (self.std1_file < len(self.pcadata['Standard1'])-1):
            self.std1f.close()
            self.std1_file += 1
            if self.verbosity > 1: 
                print("Switch Std1 file.")
            self.std1f = pyfits.open(self.pcadata['Standard1'][self.std1_file])
            self.std1f_ind = int(0)
            self.std1_arr_ind = int(0)
            self.std1t = self.std1f[1].data.field('Time')
            self.std1f_len = len(self.std1t)
            self.xepcu0 = self.std1f[1].data.field('XeCntPcu0')
            self.xepcu1 = self.std1f[1].data.field('XeCntPcu1')
            self.xepcu2 = self.std1f[1].data.field('XeCntPcu2')
            self.xepcu3 = self.std1f[1].data.field('XeCntPcu3')
            self.xepcu4 = self.std1f[1].data.field('XeCntPcu4')
            self.vpcnt = self.std1f[1].data.field('VpCnt')
            self.remcnt = self.std1f[1].data.field('RemainingCnt')
            self.vlecnt = self.std1f[1].data.field('VLECnt')
            t = max(t,self.std1t[self.std1f_ind])

        return t

    def finalize(self):
        
        try:
            self.std1f.close()
            self.filt.close()
            self.pcu2hk.close()
        except:
            pass
        
        
    # def switch_file(mode)
        
        
    
            
        
    