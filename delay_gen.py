import os
import numpy as np
import astropy.io.fits as pyfits
#from xtefilt import xtefilt
from utils import *
from xteobj_mch import *
from copy import copy
                
def phase_delay(
    info,
    modes=['auto'],
    outfile='pds.fps',
    rebin=1.01,
    fnyq=256.0,
    fft_int=256.0,
    expr="ELV > 5.0 and OFFSET < 0.1 and NUM_PCU_ON > 0",
    dtcor=True,
    chans=[(0,49),(50,255)],
    dtmethod='vikhl',
    norm='rms',
    verbosity=1,
    show_progress_bar = True
):

    """
    xtepds - procedure to calculate the power density spectrum (PDS) for XTE 
    observation which is specified by the info dictionary parameter and is either. 

    Parameter[default]:

    modes['auto'] - XTE modes to use in PDS calculation. 'auto' means modes
                    are calculated using pyxte.xteobs.suggest_fft_modes() function


    outfile['pds.fits'] - name of output file containing power spectrum.
                          Empty string '' or 'none' means do not produce any output file.

    rebin[1.02]  -   log rebinning factor (similar to POWSPEC rebin parameter)

    fnyq [256.0] -   Nyquist frequency of the resulting PDS in Hertz.

    fft_int[256.0] - Interval for individual FFTs (sec).

    expr["ELV > 5.0 and OFFSET < 0.1 and NUM_PCU_ON > 0"]
                   - data screening expression. If empty, no screening will be done.

    chans[0,255] -   lowest and highest PCA channels (0-255) to include in analysis.
                     If data mode does not allow the exact channel range to be
                     extracted, only mode bins which are completely covered by
                     the specified PCA range will be used.
    
    gtifile[''] -    Good Time Intervals (GTI) file. 
                     Empty string '' or 'none' means do not use GTI file.

    norm['rms'] -    PDS normalization (!!!to be explained!!!)

    dtcor [True] -   
                     if True, then perform dead time correction
    dtmethod['zhang'] -
                     method for deadtime correction. Can also be "vikhl".
                     '' or 'none' means no dead time cerrection
    
    """
    
    if verbosity>0:
        print("=============================================================")
        print("XTEPDS (XTE Power spectrum extractor)")
        print("Author: Nikolai Shaposhnikov, shaposhnikov@gmail.com")
        print("=============================================================")

    bin_size = 0.5/fnyq
    fft_arr_size = int(fft_int/bin_size)
    xav = np.zeros(fft_arr_size//2-1)
    x2av = np.zeros(fft_arr_size//2-1)
    fft_arr = np.zeros(fft_arr_size,dtype=int)
    if modes.__class__.__name__ == 'str': modes = [modes]

    if verbosity>0:
        print("       Bin Size",bin_size," seconds")
        print("       Nyquist frequency:",fnyq," Hz")
        print("       FFTs will be calculated for %f sec. intervals and powers"%fft_int)
        print("       averaged. Resuting PDS will be rebinned with %f rebinning factor."%rebin)
        print("------------------------------------------------------------------------")

    freqt = np.fft.fftfreq(int(fft_int/bin_size),bin_size)
    freq= freqt[1:fft_arr_size//2]
#    freq = fnyq*array(range(1,fft_arr_size/2))/(fft_arr_size/2.0)
    df = freq[2]-freq[1]
    nph = 0
    ngamma = 0
    nrange = 0

# Making GTI from filter file

    filter_file = info['filter']

    if not os.path.exists(filter_file):
        #filter_file = xtefilt(xobs)
        print(f"Filter file {filter_file} is not found!")
        print("No deadtime correction or good time intervals filtering will be done.")
        dtcor = False
    else:
        filtgti = makegti(filter_file,expr)
        xfl = filter_file
       

    if modes[0] == 'auto':
        
        if verbosity>0: print("Selecting PCA modes for PDS calculation.")
        fft_modes = suggest_fft_modes(info,verbosity=0)
        if fft_modes == -1:
            print("Failed to find proper modes.")
            return -1
        if verbosity > 0:
            print("Using following PCA modes:")
            for m in fft_modes:
                print("      %s"%m)
        print("------------------------------------------------------------------------")

    else:
        
        fft_modes = modes

    #initialize data generator
    #gen = xtegen(
    gen = xtegen_mch(
        info = info,
        fft_modes = fft_modes,
        bin_size = bin_size,
        interval = fft_int,
        channels = chans,
        expr = expr
    )
    
    bin_size = gen.bin_size
    fnyq = 0.5/bin_size

    #if verbosity>0:
    #    print("Maximum time resolution: %f"%maxres)



#    print chns
    time = gen.time

    if show_progress_bar:

        from ipywidgets import FloatProgress, HTML, VBox, Label, Layout
        from IPython.display import display

        progress = FloatProgress(
            value=gen.ftime[0],
            min = gen.ftime[0],
            max = gen.ftime[-1],
            description =  'Progress:',
            bar_style = 'info',
            orientation = 'horizontal',
            layout = Layout(width = '80%')
        )
        label = Label(value = f'0/{gen.ftime[-1]-gen.ftime[0]} sec')
        
        box = VBox(children=[label,progress])
        display(box)
        
        
        
    while not gen.stopped:

        
        ngamma = 0  
        #print(filt_ind,gen.skip)    
        input_arrays = next(gen)
        #print(input_arrays.shape)
        fft_arr1 = input_arrays[0,:]
        fft_arr2 = input_arrays[1,:]
        
        if not gen.skip:
            
            ngamma1 = np.sum(fft_arr1) 
            ngamma2 = np.sum(fft_arr2)
            nph += ngamma1 + ngamma2

#  Accumulating data for deatime correction  from HK and filter file

            nrange += 1
            ftrans1 = np.fft.fft(fft_arr1 -float(ngamma1) * np.ones(fft_arr_size,dtype=float)/float(fft_arr_size))
            ftrans2 = np.fft.fft(fft_arr2 -float(ngamma2) * np.ones(fft_arr_size,dtype=float)/float(fft_arr_size))
            power   = np.angle(np.multiply(ftrans1[1:(fft_arr_size//2)],np.conj(ftrans2[1:(fft_arr_size//2)])))
            xav = xav + power
            x2av = x2av + power*power

#        End interval loop
  
        if verbosity > 0 and not show_progress_bar:
            if gen.skip == 0:
                print(" -- START = %f, Rate1 = %f, Rate2 = %f"%(gen.time-fft_int,ngamma1/fft_int,ngamma2/fft_int))
            else:
                print(" -- START = %f, Skipped (code %i). %s"%(gen.time-fft_int,gen.skip,gen.skip_message))
        if show_progress_bar:
            progress.value = gen.ftime[gen.filt_ind]
            label.value = f'{gen.ftime[gen.filt_ind]-gen.ftime[0]}/{gen.ftime[-1]-gen.ftime[0]} s'
                    
    if verbosity>0:
        print('-----XTEPDS finished data pass. Finalizing PDS ---------')
    if show_progress_bar:
        progress.value = gen.ftime[-1]
        label.value = f'{gen.ftime[-1]-gen.ftime[0]}/{gen.ftime[-1]-gen.ftime[0]} s'

#print "Total intervals:", nrange

    xav = xav/float(nrange)
    x2av = x2av/float(nrange)
    sigma = np.zeros(fft_arr_size//2-1)
    sigma = np.sqrt(abs(x2av - xav*xav)/nrange)
#print sigma
    xax_e = np.zeros(fft_arr_size//2-1)
    xax_e[1:] = (freq[1:]-freq[0:len(freq)-1])*0.5
    xax_e[0] = (freq[1]-freq[0])*0.5


#   REBINNING

    freq_reb = []
    xax_e_reb = []
    power_reb = []
    error_reb = []

    i = int(0)
    co = 1.0

    while i < len(freq):
        
        f1 = freq[i]-xax_e[i]
        f2 = freq[i]+xax_e[i]
        pow = 0.0
        err = 0.0
        l = 0
        nn = int(co)

        while l <= nn and i < len(freq):

            f2 = max(f2,freq[i]+xax_e[i])
            pow += xav[i]
            err += sigma[i]
            i += 1
            l += 1
        
        freq_reb.append(0.5*(f1+f2))
        xax_e_reb.append(0.5*(f2-f1))
        power_reb.append(pow/float(l))
        error_reb.append(err/float(l)/np.sqrt(float(l)))

        co = co*rebin
        
    if verbosity>0: 
        print("%i frequency bins are rebinned into %i."%(len(freq),len(freq_reb)))
        print("-------------------------------------------------------------------")

# Dead time correction

    power_reb_cor = copy(power_reb)

   
#            print "End dead time corr"

#    power_reb_cor = zeros(len(power_reb),dtype=float)
    if norm == 'rms':
        coeff = fft_int*float(nrange)/float(nph)
        for i in range(len(power_reb_cor)):
            power_reb_cor[i] = coeff*power_reb_cor[i]
            power_reb[i] = coeff*power_reb[i]
            error_reb[i] = coeff*error_reb[i]


    if (outfile != '' and outfile != 'none'):

        fr_col = pyfits.Column(name='FREQUENCY', format='E', array = freq )
        xax_e_col = pyfits.Column(name='XAX_E', format='E', array = xax_e )
        pow_col = pyfits.Column(name='POWER', format='E', array = xav )
        error_col = pyfits.Column(name='ERROR', format='E', array = sigma )

        fr_col_reb = pyfits.Column(name='FREQUENCY', format='E', array = freq_reb )
        xax_e_col_reb = pyfits.Column(name='XAX_E', format='E', array = xax_e_reb )
        pow_col_reb = pyfits.Column(name='POWER_NODTCOR', format='E', array = power_reb )
        pow_col_reb_cor = pyfits.Column(name='POWER', format='E', array = power_reb_cor )
        error_col_reb = pyfits.Column(name='ERROR', format='E', array = error_reb )


        cols = pyfits.ColDefs([fr_col, xax_e_col, pow_col, error_col])
        cols_reb  = pyfits.ColDefs([fr_col_reb, xax_e_col_reb, pow_col_reb, pow_col_reb_cor, error_col_reb])
        
        hdr = pyfits.Header()
        hdr['EXTNAME'] = 'XTE_PDS'
        hdr['OBSID'] = info['id']
        hdr['TARGET'] = info['target']
        primary_hdu = pyfits.PrimaryHDU(header=hdr)
        
        tbhdu = pyfits.BinTableHDU.from_columns(cols)

        tbhdu1 = pyfits.BinTableHDU.from_columns(cols_reb)
#        tbhdu1.header[''] = 


        fps_hdu_list = pyfits.HDUList([primary_hdu,tbhdu1])

        if os.path.exists(outfile):

            print("File ",outfile," exists. Writing into",str(os.getpid())+outfile)
            outfile = str(os.getpid())+outfile

        fps_hdu_list.writeto(outfile)    

    gen.finalize()

    print("---XTEPDS Finished -------------------------------------------------------")

    return power_reb_cor,error_reb,freq_reb,xax_e_reb,outfile


#  END of the xtepds function
